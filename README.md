# README for drop-videos

## Overview
Drop-videos is a small project for personal use, that displays an icon that acts as a drag-and-drop target that invokes either a downloader or a viewer, depending on how you invoked the program. The main utility is `drop-videos.sh` which displays a small icon that lets you drop video links on it. Depending on the environment variables at invocation, the program will download/view the links.

## Using
Several example scripts are provided, which is every script in this directory excluding `drop-videos.sh`. My main use cases include the following provided examples.
Use case                                | file
--------------------------------------- | --------------
Download all videos from this playlist  | `save-playlist.sh`
Download the specific link              | `save-audio.sh`
View the specific link                  | `view-video.sh`

The application accepts the standard environment variables DEBUG and DRYRUN.

## Upstreams
### Dragon
This project uses a customized [dragon](https://github.com/mwh/dragon) binary. The diff is provided here, as well as the customized source code.

## Alternatives
I read article [Open YouTube (And More) Videos From Your Web Browser With mpv [Firefox, Chrome] - Linux Uprising Blog](https://www.linuxuprising.com/2021/07/open-youtube-and-more-videos-from-your.html) which describes how to set up a browser extension that lets you add a userscript that shows a small button in a youtube browser page that opens that video in a local video player. This worked on my systems (aside from the fact that I don't have mpv installed so the protocol handler fails), but I wanted a solution that would work without having to visit each and every link first. This article that inspired me refers to [Tampermonkey](https://www.tampermonkey.net/) which appears to be a fork of [greasemonkey](https://github.com/janekptacijarabaci/greasemonkey/releases/tag/3.31.4Fork) which has a fork for Palemoon. This Greasemonkey can use the [userscript](https://greasyfork.org/scripts/416271-play-with-mpv) that will introduce a custom protocol which you can configure the OS to send to a specific program.

## Dependencies
### Compiling
To compile the C binary, you need gtk3 libraries.

### Running
* Bourne shell
* youtube-dl
* vlc or other media player

## References

## Differences from upstream
N/A
