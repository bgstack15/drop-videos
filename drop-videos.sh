#!/bin/sh
# File: drop-videos.sh
# Location: https://gitlab.com/bgstack15/drop-videos
# Author: bgstack15
# SPDX-License-Identifier: GPL-3.0
# Startdate: 2021-07-22 09:42
# Title: Drop Videos Here To Save/View Them
# Purpose: Save down with youtube-dl all links that are dropped onto dragon-drag-and-drop
# Usage: See README.md
# Reference:
#    https://stackoverflow.com/questions/7970617/how-can-i-check-if-char-variable-points-to-empty-string
#    https://www.gnu.org/software/libc/manual/html_node/Environment-Access.html
# Improve:
# Environment vars:
#    DV_COMMAND DV_DRAGON_BIN DV_OUTDIR DV_YTDL_BIN DV_YTDL_OPTS DV_VIEWER_BIN DV_VIEWER_OPTS DRYRUN DEBUG
# Dependencies:
#    customized dragon-drag-and-drop which is in this source tree

# available COMMANDs are save and view
test -z "${DV_COMMAND}" && DV_COMMAND="save"
test -z "${DV_DRAGON_BIN}" && DV_DRAGON_BIN="$( dirname "$( readlink -f "${0}" )" )/dragon"
test -z "${DV_OUTDIR}" && DV_OUTDIR=/mnt/public/Video/temp
test -z "${DV_YTDL_BIN}" && DV_YTDL_BIN="$( which youtube-dl )"
test -z "${DV_YTDL_OPTS}" && DV_YTDL_OPTS=""
test -z "${DV_VIEWER_BIN}" && DV_VIEWER_BIN="$( which vlc )"
test -z "${DV_VIEWER_OPTS}" && DV_VIEWER_OPTS=""

# INITIALIZE
export DV_COMMAND DV_DRAGON_BIN DV_OUTDIR DV_YTDL_BIN DV_YTDL_OPTS DV_VIEWER_BIN DV_VIEWER_OPTS
cd "${DV_OUTDIR}"

# VALIDATE INPUT
case "${DV_COMMAND}" in
   "view") test -z "${DRAGON_ICON}" && export DRAGON_ICON="$( basename "${DV_VIEWER_BIN}" )" ;;
   "save") test -z "${DRAGON_ICON}" && export DRAGON_ICON="$( basename "${DV_YTDL_BIN}" )" ;;
   *) echo "Invalid DV_COMMAND. Choose from ['view','save']. Aborted." ; exit 1 ;;
esac

# MAIN
"${DV_DRAGON_BIN}" --target --on-top --icon-only | while read line ;
do
   case "${DV_COMMAND}" in
      "view")
         test -n "${DEBUG}" && echo "${DV_VIEWER_BIN} ${DV_VIEWER_OPTS:+${DV_VIEWER_OPTS} }${line}"
         test -z "${DRYRUN}" && "${DV_VIEWER_BIN}" ${DV_VIEWER_OPTS:+${DV_VIEWER_OPTS} }"${line}"
         ;;
      "save")
         test -n "${DEBUG}" && echo "${DV_YTDL_BIN} ${DV_YTDL_OPTS:+${DV_YTDL_OPTS} }${line}"
         test -z "${DRYRUN}" && "${DV_YTDL_BIN}" ${DV_YTDL_OPTS:+${DV_YTDL_OPTS} }"${line}"
      ;;
   esac
done
